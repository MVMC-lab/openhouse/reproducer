#include "ASA_Lib.h"

#define OpenFileAppend 0x08
#define OpenFileWrite 0x04
#define OpenFileRead 0x02
#define CloseFile 0x01

#define FileRdWrLSByte 0
#define GetLSByteStart 100
#define FileSetLSByte 200
#define FileIDLSByte 201


#define TempDataSize 100

#define ASA_SDC_ID 3


//-----------------SDC參數----------------------//
unsigned int File_ID = 4;
unsigned char FileSetReg, Mask = 0xFF, Shift = 0, Setting = 0xFF,Bytes=2;
char check;
unsigned int FileData;
volatile int SDC_read;

//-----------------ADC參數----------------------//
volatile long int ADC_count=0,second;
unsigned int ADC_Data;

//-----------------DAC參數----------------------//
volatile int DAC_count=0;
int loop=0,start=1,play,i,record_new,play_again=1;
//double MatlabData[8], DACdata[64];
unsigned int DACinput;


//------------functions-------------------------//
void func(void){

    //printf("interupt\n");
    if(SDC_read == 0)											//ADC卡紀錄波型
    {
        ASA_ADC00_set(2,200,0x20,5,0x00);//通道0 保值
        ASA_ADC00_set(2,200,0xC0,6,0x03);//通道3 保值
        ASA_ADC00_set(2,200,0xC0,6,0x00);  //ASA_ADC00_set(ID, LSByte,MASK,Shift,Data) //通道0 取樣
        ASA_ADC00_get(2,101,2,&ADC_Data);  //ASA_ADC00_set(ID, LSByte,Bytes,data_p)

        if(ADC_count<second)
        ASA_SDC00_put(ASA_SDC_ID,FileRdWrLSByte,Bytes,&ADC_Data);

        ADC_count++;
    }


    else if(SDC_read == 1)
    {

        check = ASA_SDC00_get(ASA_SDC_ID, GetLSByteStart,Bytes,&FileData);

        ASA_DAC00_put(1,000,2,&FileData);			//DAC卡輸出波型

    }

}

int main(void)
{

    //-----------------------------------------------ADC與SDC程式-------------------------------------------//
    //double get[100];

    ASA_M128_set();
    while(1)
    {
        ASA_ADC00_set(2, 200, 0xC0, 6, 0x00); //通道0
        ASA_ADC00_set(2, 200, 0x20, 5, 0x01); //保值0/取樣1

        ASA_DAC00_set(1, 200, 0x80, 7, 0x01); //單通道非同步模式
        ASA_DAC00_set(1, 200, 0x40, 6, 0x00); //同步輸出0
        ASA_DAC00_set(1, 200, 0x30, 4, 0x00); //輸出通道0 S1S2

        TIM_set();
        TIMISR_reg(func);

        SDC_read =0;
        ADC_count = 0;
        _delay_ms(10);

        while(start == 1)
        {
            printf("------which file number do you want to record------? (please input a number 1-5)\n");
            scanf("%d",&File_ID);
            File_ID = File_ID;

            printf("------How many seconds do you want to record?------? (please input a number)\n");
            scanf("%ld",&second);
            second = second*6400;

            ASA_SDC00_put(ASA_SDC_ID,FileIDLSByte,Bytes,&File_ID);	//ID,LSByte,Mask,Data  檔案編號設定為4開始 //設定開檔編號
            Setting = OpenFileWrite;									//設定為開檔寫檔
            ASA_SDC00_set(ASA_SDC_ID, FileSetLSByte, Mask, Shift, Setting);	//ID,LSByte,Mask,Data  開檔4蓋寫
            M128_AllINT_enable;

            printf("RECORDING...\n");
            while(ADC_count < second)
            {;/*蓋寫檔案*/}

            M128_AllINT_disable;
            // printf("\033[0;0H\033[2J");											//中斷禁能
            printf("Record END\n");

            Setting = CloseFile;											/*關閉檔案*/
            ASA_SDC00_set(ASA_SDC_ID,FileSetLSByte,Mask,Shift,Setting);

            printf("------Do you want to record voice again------? 1(YES)/0(NO)\n");
            scanf("%d",&start);
        }


        /*檔案讀取*/
        while(play_again == 1)
        {
            printf("------which file number do you want to PLAY------? (please input a number 1-5)\n");
            scanf("%d",&File_ID);
            File_ID = File_ID;

            Bytes = 2;
            Setting = File_ID & 255;												//設定開檔編號
            check = ASA_SDC00_set(ASA_SDC_ID, FileIDLSByte, Mask, Shift, Setting);	//ID,LSByte,Mask,Data  檔案4開檔取得檔案低byte
            //	printf("checkpoint1 = %d\n",check);
            Setting = File_ID >> 8;
            check = ASA_SDC00_set(ASA_SDC_ID, FileIDLSByte+1, Mask, Shift, Setting);//ID,LSByte,Mask,Data  檔案4開檔取得檔案高byte
            //printf("checkpoint2 = %d\n",check);

            Setting = OpenFileRead;													//設定為開檔讀檔
            check = ASA_SDC00_set(ASA_SDC_ID, FileSetLSByte, Mask, Shift, Setting);
            //printf("checkpoint3 = %d\n",check);

            //printf("------press any button to start to play------?\n");
            //scanf("%d",&play);

            M128_AllINT_enable;												//致能中斷

            SDC_read = 1;
            /*讀取檔案資料*/
            while(check == 0)
            {printf("PLAYING...\n");}

            /*關閉檔案*/
            Setting = CloseFile;
            check = ASA_SDC00_set(ASA_SDC_ID, FileSetLSByte, Mask, Shift, Setting);
            M128_AllINT_disable;
            // printf("\033[0;0H\033[2J");
            printf("READ END\n");
            printf("--------play again(Y1/N0)?--------\n");
            scanf("%d",&play_again);
        }
        start=1;
        play_again=1;
    }

    return 0;
}
